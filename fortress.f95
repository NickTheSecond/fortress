PROGRAM FORTRESS
USE gui 
USE regex_module 
IMPLICIT NONE
    CHARACTER(LEN=4), PARAMETER :: save_scrn="/tmp"
    CHARACTER, DIMENSION(5) :: pressed_key
    CHARACTER(:), ALLOCATABLE :: scrn1
    LOGICAL :: looping, stepping, running=.FALSE., no_graphics=.FALSE.
    INTEGER :: x, y, i, j, k, skip_size, x_offset, y_offset, max_generations = 420
    ! CHARACTER(255) :: filename = "wireworldFiles/NickGardner.rle"
    CHARACTER(255) :: arg, outfilename = " ", infilename = "wireworldFiles/circuit.rle"
    INTEGER, DIMENSION(2) :: board_shape
    INTEGER, DIMENSION(:,:), POINTER :: board1_ptr, board2_ptr

    i = 1
    DO
        CALL GET_COMMAND_ARGUMENT(i, ARG)
        IF (LEN(TRIM(ARG)) == 0) EXIT
        IF (arg == "--no-graphics") THEN 
            no_graphics = .TRUE.
        ELSE IF (INDEX(ARG, "--in=") == 1) THEN
            infilename = arg
        ELSE IF (INDEX(ARG, "--out=") == 1) THEN
            outfilename = arg
        END IF
        i = i + 1
    END DO

    CALL INIT_BOARDS(board1_ptr, board2_ptr, infilename)
    ! WRITE(*,*) SIZE(board2_ptr), SHAPE(board2_ptr)
    ! WRITE(*,*) SIZE(board1_ptr), SHAPE(board1_ptr)
    WRITE(*,'(A)') "Loaded!"

    board_shape = SHAPE(board1_ptr)    
    y = board_shape(1)
    x = board_shape(2)

    ! Make sure the board is not dead.
    DO i=1,y
        DO j=1,x
            IF (board1_ptr(i, j) /= 0) running = .TRUE.
        END DO
    END DO
    IF (running .EQV. .FALSE.) THEN
        WRITE(*,*) "Board is all dead, nothing to show."
        STOP
    END IF

    IF (no_graphics) THEN
        DO i=1,max_generations
            CALL UPDATE_CELLS(board1_ptr, board2_ptr, x, y)
        END DO
        STOP
    ELSE
        CALL INIT_SCREEN(save_scrn)
        ALLOCATE(CHARACTER(LEN=lines*cols) :: scrn1)
        scrn1=clear_screen

        x_offset = 1
        y_offset = 1
        pressed_key = " "

        looping = .FALSE. ! we want to keep going indefinitely
        stepping = .TRUE. ! we are in the middle of taking 1 step
        skip_size = 1
        k = 1
        DO WHILE(running)
            !WRITE(*,*) "x:", x, "y:", y
            IF (KBHIT() .EQ. 1) THEN
                CALL GETKEY(pressed_key)
                !WRITE(*, '(A4,5I3.3,X,5Z2.2,X,6A)') "key:", IACHAR(pressed_key), &
                !                                    pressed_key, pressed_key, ACHAR(13)
                SELECT CASE(pressed_key(1))
                    CASE(CHAR(32))  ! space
                        looping = .FALSE.
                        stepping = .TRUE.
                    CASE(CHAR(115)) ! 's'
                        looping = .TRUE.
                    CASE(CHAR(113)) ! 'q'
                        EXIT
                    CASE(CHAR(27)) ! pan with arrow keys... sketchily
                        IF (pressed_key(2) == CHAR(91)) THEN
                            SELECT CASE(pressed_key(3))
                                CASE(CHAR(65)) ! up arrow
                                    y_offset = y_offset - 1
                                CASE(CHAR(66)) ! down arrow
                                    y_offset = y_offset + 1
                                CASE(CHAR(68)) ! left arrow
                                    x_offset = x_offset - 1
                                CASE(CHAR(67)) ! right arrow
                                    x_offset = x_offset + 1
                            END SELECT

                            IF (x_offset + cols > cols) x_offset = x - cols
                            IF (y_offset + lines > lines) y_offset = y - lines
                            IF (x_offset < 1 .OR. x <= cols) x_offset = 1
                            IF (y_offset < 1 .OR. y <= lines) y_offset = 1
                        END IF
                    END SELECT
                CALL DRAW(board1_ptr(y_offset:y_offset+MIN(y, lines), x_offset:x_offset+MIN(x, cols)), scrn1, k)
            END IF

            IF (stepping) THEN
                CALL UPDATE_CELLS(board1_ptr, board2_ptr, x, y)
                CALL DRAW(board1_ptr(y_offset:y_offset+MIN(y, lines), x_offset:x_offset+MIN(x, cols)), scrn1, k)
                stepping = .FALSE.
                k = k + 1
            ELSE IF (looping) THEN
                CALL UPDATE_CELLS(board1_ptr, board2_ptr, x, y)
                CALL DRAW(board1_ptr(y_offset:y_offset+MIN(y, lines), x_offset:x_offset+MIN(x, cols)), scrn1, k)
                !CALL SLEEP(1)
                stepping = .FALSE.
                k = k + 1
            END IF
        END DO

        DEALLOCATE(scrn1)
        CALL KILL_SCREEN(save_scrn)

        IF (LEN(TRIM(outfilename)) > 0) CALL EXPORT_PGM(TRIM(outfilename(7:)), board1_ptr, 4)
    END IF

CONTAINS

    LOGICAL FUNCTION RE_MATCH(re, str)
        CHARACTER(LEN=:), ALLOCATABLE, INTENT(IN) :: re, str
        INTEGER :: status
        INTEGER, DIMENSION(2,1) :: matches
        TYPE(REGEX_TYPE) :: regex

        CALL REGCOMP(regex, re)
        RE_MATCH = REGEXEC(regex, str, matches)
        CALL REGFREE(regex)
    END FUNCTION

END PROGRAM
