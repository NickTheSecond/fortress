PROGRAM run_timings
USE ca_ww
USE hashing_states
IMPLICIT NONE

    INTEGER :: x, y, i
    INTEGER, PARAMETER :: num_runs = 100000
    REAL :: start_c, finish_c, start_h, finish_h
    CHARACTER(255) :: filename
    INTEGER, DIMENSION(2) :: board_shape
    INTEGER, DIMENSION(:,:), POINTER :: board1_ptr, board2_ptr

    filename = 'wireworldFiles/circuit.rle'

!-----------------------------------------------------------------------

    CALL INIT_BOARDS(board1_ptr, board2_ptr, filename)

    board_shape = SHAPE(board1_ptr)    
    y = board_shape(1)
    x = board_shape(2)

    CALL CPU_TIME(start_c)
        DO i = 0, num_runs
            CALL UPDATE_CELLS(board1_ptr, board2_ptr, x, y)
        END DO
    CALL CPU_TIME(finish_c)

!-----------------------------------------------------------------------

    CALL INIT_BOARDS(board1_ptr, board2_ptr, filename)

    board_shape = SHAPE(board1_ptr)    
    y = board_shape(1)
    x = board_shape(2)

    CALL CPU_TIME(start_h)
        DO i = 0, num_runs
            CALL UPDATE_CELLS_HASH(board1_ptr, board2_ptr, x, y)
        END DO
    CALL CPU_TIME(finish_h)

    print '("Normal Time = ",f6.3," seconds.")',finish_c-start_c

    print '("Hash Time = ",f6.3," seconds.")',finish_h-start_h

    DEALLOCATE(board1_ptr)
    DEALLOCATE(board2_ptr)
END PROGRAM run_timings