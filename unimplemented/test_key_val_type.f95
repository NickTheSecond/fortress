MODULE test_key_val_type
USE test_key
IMPLICIT NONE

#define FFH_KEY_TYPE type(TestKey)
#define FFH_CUSTOM_KEYS_EQUAL
#define FFH_VAL_TYPE type(TestKey)
#include "ffhash_inc.f90"

    PURE LOGICAL FUNCTION keys_equal(a, b)
        TYPE(TestKey), INTENT(IN) :: a, b
        keys_equal = (a%s == b%s)
    END FUNCTION keys_equal
end module test_key_val_type

