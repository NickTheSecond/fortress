MODULE vn_cell_type
IMPLICIT NONE

    TYPE VNCell
        ! 0 = None, 1 = Up, 2 = Right, 3 = Down, 4 = Left
        INTEGER :: direction
        ! 0 Ground, 1-8 Wiki Sensitized, 9-12 Wiki Confluent
        !13 - 14 Ordinary Trans Exc then Quin, 15 - 16 Special Trans E then Q 
        INTEGER :: state
    END TYPE VNCell
END MODULE vn_cell_type

MODULE ca_vn
USE vn_cell_type
IMPLICIT NONE

CONTAINS 
SUBROUTINE UPDATE_VN_CELLS(board_ptr, new_board_ptr, x, y) 
IMPLICIT NONE
    
LOGICAL :: given_data
INTEGER :: i, j, m, n

SUBROUTINE UPDATE_VN_CELLS

SUBROUTINE INIT_VN_BOARDS()
IMPLICIT NONE
    TYPE(VNCell), DIMENSION(:,:), POINTER, INTENT(INOUT) :: board1, board2

SUBROUTINE INIT_VN_BOARDS
END MODULE ca_vn