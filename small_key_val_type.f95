MODULE small_key
IMPLICIT NONE

    TYPE SmallKey
        CHARACTER(len = 144) :: s
    END TYPE SmallKey
END MODULE small_key

MODULE small_key_val_type
USE small_key
IMPLICIT NONE

#define FFH_KEY_TYPE type(SmallKey)
#define FFH_CUSTOM_KEYS_EQUAL
#define FFH_VAL_TYPE type(SmallKey)
#include "ffhash_inc.f90"

    PURE LOGICAL FUNCTION keys_equal(a, b)
        TYPE(SmallKey), INTENT(IN) :: a, b
        keys_equal = (a%s == b%s)
    END FUNCTION keys_equal
end module small_key_val_type