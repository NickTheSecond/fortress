MODULE hashing_states
USE ca_ww
USE cell_type
USE test_key
USE test_key_val_type
IMPLICIT NONE

CONTAINS
    FUNCTION CALC_WIRE(nbr_cells) RESULT (state)
    IMPLICIT NONE
        INTEGER, DIMENSION(3, 3), INTENT (IN) :: nbr_cells

        INTEGER :: head_count, i, j
        INTEGER(kind = 2) :: state

        head_count = 0

        DO i = 1, 3
            DO j = 1, 3
                IF (nbr_cells(i, j) == 1) THEN
                    head_count = head_count + 1
                END IF
            END DO
        END DO

        IF (head_count == 1 .OR. head_count == 2) THEN
            state = 1
        ELSE
            state = 3
        END IF
    END FUNCTION CALC_WIRE

    SUBROUTINE CALC_STATES(board_ptr, new_board_ptr, x, y)
    IMPLICIT NONE

        INTEGER, INTENT(IN) :: x, y
        INTEGER :: i, j
        INTEGER, DIMENSION(:,:), ALLOCATABLE, INTENT (INOUT) :: board_ptr, new_board_ptr

        DO i = 1, y - 2
            DO j = 1, x - 2
                SELECT CASE (board_ptr(i, j))
                    CASE (0)
                        new_board_ptr(i, j) = board_ptr(i, j)
                    CASE (1:2)
                        ! none -> none ; head -> tail ; tail -> wire
                        new_board_ptr(i, j) = board_ptr(i, j) + 1
                    CASE(3)
                        new_board_ptr(i, j) = CALC_WIRE(board_ptr(i-1:i+1, j-1:j+1))
                END SELECT
            END DO
        END DO

        new_board_ptr = new_board_ptr(2:y-2, 2:x-2)

    END SUBROUTINE CALC_STATES

    FUNCTION FETCH_STATES(super_cell, state_patterns_ptr, hashing_factor) RESULT (updated_super_cell)
    IMPLICIT NONE

        INTEGER, DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: super_cell
        TYPE(TestKey) :: key, value
        TYPE(ffh_t), INTENT(INOUT) :: state_patterns_ptr
        INTEGER, INTENT(IN) :: hashing_factor
        INTEGER, DIMENSION(:,:), ALLOCATABLE :: updated_super_cell
        INTEGER :: i, j, counter, status

        ALLOCATE(updated_super_cell(hashing_factor, hashing_factor))

        DO i = 0, hashing_factor + 2
            DO j = 0, hashing_factor + 2
                IF (super_cell(j, i) == 0) THEN
                    key%s(1:1) = "0"
                ELSE IF (super_cell(j, i) == 3) THEN
                    key%s(1:1) = "3"
                ELSE IF (super_cell(j, i) == 1) THEN
                    key%s(1:1) = "1"
                ELSE IF (super_cell(j, i) == 2) THEN
                    key%s(1:1) = "2"
                END IF
            END DO
        END DO

        status = state_patterns_ptr%get_index(key)

        IF (status /= -1) THEN
            value = state_patterns_ptr%vals(status)
            counter = 1
            DO i = 0, hashing_factor
                DO j = 0, hashing_factor
                    IF (value%s(counter:counter) == '0') THEN
                        updated_super_cell(j, i) = 0
                    ELSE IF (value%s(counter:counter) == '3') THEN
                        updated_super_cell(j, i) = 3
                    ELSE IF (value%s(counter:counter) == '1') THEN
                        updated_super_cell(j, i) = 1
                    ELSE IF (value%s(counter:counter) == '2') THEN
                        updated_super_cell(j, i) = 2
                    END IF
                    counter = counter + 1
                END DO
            END DO
        ELSE
            value%s = ""

            CALL CALC_STATES(super_cell, updated_super_cell, hashing_factor + 2, hashing_factor + 2)

            DO i = 0, hashing_factor
                DO j = 0, hashing_factor
                    SELECT CASE (updated_super_cell(j, i))
                        CASE (0)
                            value%s = value%s // '0'
                        CASE (3)
                            value%s = value%s // '3'
                        CASE (1)
                            value%s = value%s // '1'
                        CASE (2)
                            value%s = value%s // '2'
                    END SELECT
                END DO
            END DO

            CALL state_patterns_ptr%ustore_value(key, value)
        END IF
    END FUNCTION FETCH_STATES

    SUBROUTINE UPDATE_CELLS_HASH(board_ptr, new_board_ptr, x, y)
    IMPLICIT NONE
        INTEGER, DIMENSION(:,:), POINTER, INTENT(INOUT) :: board_ptr, new_board_ptr
        INTEGER, DIMENSION(:,:), ALLOCATABLE :: super_cell, updated_super_cell
        INTEGER, INTENT (IN) :: x, y

        type(ffh_t), POINTER :: state_patterns_ptr
        INTEGER, PARAMETER :: hashing_factor = 2
        INTEGER :: i, j

        ALLOCATE(super_cell(2+hashing_factor, 2+hashing_factor))

        DO i = 1, y, hashing_factor
            DO j = 1, x, hashing_factor
                super_cell = board_ptr(i:i+2+hashing_factor, j:j+2+hashing_factor)
                updated_super_cell = FETCH_STATES(super_cell, state_patterns_ptr, hashing_factor)
                new_board_ptr(i+1:i+1+hashing_factor, j+1:j+1+hashing_factor) = updated_super_cell
                DEALLOCATE(updated_super_cell)
            END DO
        END DO


    END SUBROUTINE UPDATE_CELLS_HASH


END MODULE hashing_states
