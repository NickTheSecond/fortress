MODULE read_ww
    ! PROGRAM read_ww
    USE cell_type
    IMPLICIT NONE

    CONTAINS
        SUBROUTINE GET_XY(filename, w, h)
            INTEGER, INTENT(INOUT) :: w, h 
            CHARACTER(255), INTENT(IN) :: filename
            CHARACTER(50) :: garb, wS, hS

            OPEN(UNIT=12, FILE=filename, STATUS="old", ACTION="read")
            READ(12, *)
            READ(12, *) garb, garb, wS, garb, garb, hS, garb, garb, garb
            READ(wS(:LEN(wS)-1),*) w
            READ(hS(:LEN(hS)-1),*) h
            w = w + 2
            h = h + 2
            w = INT(2**CEILING(LOG(REAL(MAX(w, h)))/LOG(2.0)))
            h = w
    
            ! WRITE(*,*) "w,h,:", w, h
            CLOSE(12)
        END SUBROUTINE


        SUBROUTINE PRINT_BOARD(board_ptr)
            INTEGER :: x, y, s
            INTEGER, DIMENSION(:,:), POINTER, INTENT(IN) :: board_ptr
            CHARACTER(:), ALLOCATABLE :: line
            INTEGER, DIMENSION(2) :: board_shape
            CHARACTER(10) newChar
            board_shape = SHAPE(board_ptr)
            ALLOCATE(CHARACTER(board_shape(2)) :: line)
            line = ""
            newChar = ""
            DO x = 1, board_shape(2)
                DO y = 1, board_shape(1)
                    s = board_ptr(y,x)
                    WRITE(newChar, *) s
                    line = line//TRIM(newChar)
               END DO
            END DO
        END SUBROUTINE
        

        SUBROUTINE GET_FILE_TEXT(filename, sDataRaw)
            INTEGER :: dataLen
            CHARACTER(255), INTENT(INOUT) :: filename
            CHARACTER(10**6) :: sDataRaw
            CHARACTER(255) :: line
            ! CHARACTER(:), ALLOCATABLE, INTENT(INOUT) :: sData
            
            OPEN(UNIT=13, FILE=filename, STATUS="old", ACTION="read")
            READ(13, *)
            READ(13, *)
            
            line = ""
            sDataRaw = ""
            dataLen = 0
            DO WHILE (INDEX(line, "!") == 0)
                READ(13, *) line
                sDataRaw = TRIM(sDataRaw)//TRIM(line)
                dataLen = dataLen + LEN(TRIM(line))
                ! WRITE(*,*) LEN(line), line
            END DO
            ! ALLOCATE(CHARACTER(LEN=dataLen) :: sData)
            ! Skipping the resizing for now
            ! sData = TRIM(sDataRaw)
            !WRITE(*,*) dataLen, sData
            CLOSE(13)
        END SUBROUTINE


        SUBROUTINE TEXT_TO_MODEL(board_ptr, sDataRaw)
            CHARACTER(:), ALLOCATABLE :: sData
            CHARACTER(1) :: front
            CHARACTER(10**6) :: sDataRaw
            INTEGER, DIMENSION(:,:), POINTER, INTENT(INOUT) :: board_ptr
            INTEGER :: multiplier, x, y, iter, sIter, newMult, xW, yW
            INTEGER(KIND=2) :: newState

            INTEGER, DIMENSION(2) :: board_shape

            ! WRITE(*,*) "Raw data:", sDataRaw

            ALLOCATE(CHARACTER(LEN(TRIM(sDataRaw))) :: sData)
            sData = TRIM(sDataRaw)

            multiplier = 0
            x = 1
            y = 1
            sIter = 1
            front = sData(sIter:sIter)
            DO WHILE (front /= "!")
                WRITE(*,*) sData(sIter:MIN(sIter+10,LEN(sDATA))), x, y, multiplier
                IF (INDEX(".ABC", front) /= 0) THEN
                    ! writing cells to graph
                    SELECT CASE (front)
                    CASE (".")
                        newState = 0
                    CASE ("A")
                        newState = 1
                    CASE ("B")
                        newState = 2
                    CASE ("C")
                        newState = 3
                    END SELECT
                    ! writing the symbol across {multiplier} cells
                    DO iter = 1, MAX(1, multiplier)
                        x = x + 1
                        ! WRITE(*,*) "Writing to: ", x, y
                        board_ptr(y,x) = newState
                        ! WRITE(*,*) "Wrote @ ", x, y
                        
                    END DO
                    multiplier = 0
                    sIter = sIter + 1
                ELSE IF (front == "$") THEN
                    ! new line
                    multiplier = 0
                    x = 1
                    y = y + 1
                    sIter = sIter + 1
                ELSE IF (front /= "!") THEN
                    ! WRITE(*,*) "Front: "//front
                    ! WRITE(*,*) sIter, sData(sIter: sIter), " ", sData(sIter:MIN(sIter+10,LEN(sData)))
                    ! reading in a number
                    ! READ(front,*) newMult
                    newMult = IACHAR(front)-48
                    multiplier = multiplier*10+newMult
                    sIter = sIter + 1

                ELSE
                    EXIT
                END IF
                front = sData(sIter:sIter)
            END DO
        END SUBROUTINE

END MODULE read_ww