MODULE cell_type
IMPLICIT NONE

    TYPE Cell
        ! 4 States:
        ! 0 = None, 1 = E Head, 2 = E Tail, 3 = Wire
        INTEGER(KIND = 2) :: state
    END TYPE Cell
    
END MODULE cell_type