MODULE large_key
IMPLICIT NONE

    TYPE LargeKey
        CHARACTER(len = 10404) :: s
    END TYPE LargeKey
END MODULE large_key

MODULE large_key_val_type
USE large_key
IMPLICIT NONE

#define FFH_KEY_TYPE type(LargeKey)
#define FFH_CUSTOM_KEYS_EQUAL
#define FFH_VAL_TYPE type(LargeKey)
#include "ffhash_inc.f90"

    PURE LOGICAL FUNCTION keys_equal(a, b)
        TYPE(LargeKey), INTENT(IN) :: a, b
        keys_equal = (a%s == b%s)
    END FUNCTION keys_equal
end module large_key_val_type