import csv

head_locations = [[8, 3], [2, 3], [3, 0], [9, 0], [15, 0], [21, 0], [27, 0], [33, 0], [39, 0], [45, 0], [44, 4], [38, 3], [32, 3], [26, 3], [20, 3], [14, 3] ]
tail_locations = [[7, 3], [1, 3], [4, 0], [10, 0], [16, 0], [22, 0], [28, 0], [34, 0], [40, 0], [46, 1], [43, 3], [37, 3], [31, 3], [25, 3], [19, 3], [13, 3] ]

changes = []

data = []
with open('computer.csv', 'r') as f:
    reader = csv.reader(f, delimiter = ',')
    for row in reader:
        newRow = [i[1:] if ' ' in i else i for i in row]
        data.append(newRow)


def getRegisterCoordinates(n):
    return [646-6*(n-1), 29+6*(n-1)]

def putBit(reg, n, bit):
    result = []
    reg_coord = getRegisterCoordinates(reg)  # upper-left corner of the register
    x0 = reg_coord[0]
    y0 = reg_coord[1]
      
    reg_index = (reg-1) % 16                # at which position the 0-th bit is located
      
    if (bit):
        idx = (n + reg_index) % 16
        x = x0 + head_locations[idx][0]
        y = y0 + head_locations[idx][1]
        result.append([x, y, 2])
        
        if (n == 0):                       # additional 'up' bit
            result.append([x, y-1, 2])
        
        x = x0 + tail_locations[idx][0]
        y = y0 + tail_locations[idx][1]
        result.append([x, y, 3])
    return result
    
def loadRegister(reg, word):
    results = []
    if (reg < 1 or reg > 52):
        return
    for i in range(16):
        bit = word & (1 << i)
        results.extend(putBit(reg, i, bit))
    return results

def modelToRLE(data, filename):
    encodings = {'0':'.', '1':'A', '2':'B', '3':'C'}
    f = open(filename, 'w')
    f.write('#CXRLE Pos=0,0\n')
    groups = []
    xMax = 0
    yMax = 0
    for i, col in enumerate(data):
        if sum([int(n) for n in col]) > 0:
            xMax = i
    for j in range(len(data[0])):
        for i in range(len(data)):
            c = data[i][j]
            if c != 0 and j > yMax:
                yMax = j
            if len(groups) == 0 or groups[-1][0] != encodings[c]:
                groups.append([encodings[c], 1])
            else:
                groups[-1][1] += 1
        groups.append(['$\n', 1])
    text = ''
    print(max([n[1] for n in groups]))
    for group in groups:
        if group[1] > 1:
            text += str(group[1])
        text += group[0]
    text = text[:-1]+'!'
    f.write('x = {}, y = {}, rule = WireWorld:P{},{}\n'.format(xMax, yMax, max(xMax,yMax), max(xMax,yMax)))
    f.write(text)
    f.close()


changes.extend(loadRegister(1, 64*0+7))
modelToRLE(data, 'addComputer.rle')

print('Done!')