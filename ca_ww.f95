MODULE ca_ww
USE cell_type
USE read_ww
IMPLICIT NONE

CONTAINS
    FUNCTION NEW_WIRE_STATE(nbr_cells) RESULT (state)
        IMPLICIT NONE

        INTEGER, DIMENSION(3, 3), INTENT (IN) :: nbr_cells

        INTEGER :: head_count, i, j
        INTEGER(kind = 2) :: state

        head_count = 0

        DO i = 1, 3
            DO j = 1, 3
                IF (nbr_cells(i, j) == 1) THEN
                    head_count = head_count + 1
                END IF
            END DO
        END DO

        IF (head_count == 1 .OR. head_count == 2) THEN
            state = 1
        ELSE
            state = 3
        END IF
        
    END FUNCTION NEW_WIRE_STATE

    SUBROUTINE UPDATE_CELLS(board_ptr, new_board_ptr, x, y)
        IMPLICIT NONE

        INTEGER, INTENT(IN) :: x, y
        INTEGER :: i, j
        INTEGER, DIMENSION(:,:), POINTER, INTENT (INOUT) :: board_ptr, new_board_ptr
        INTEGER, DIMENSION(:,:), POINTER :: temp

        DO i = 1, y - 2
            DO j = 1, x - 2
                SELECT CASE(board_ptr(i, j))
                    CASE (0)
                        new_board_ptr(i, j) = board_ptr(i, j)
                    CASE (1:2)
                        ! none -> none ; head -> tail ; tail -> wire
                        new_board_ptr(i, j) = board_ptr(i, j) + 1
                    CASE(3)
                        new_board_ptr(i, j) = NEW_WIRE_STATE(board_ptr(i-1:i+1, j-1:j+1))
                END SELECT
                !IF (board_ptr(i, j)%state == 0) THEN
                ! If electron head, then becomes electron tail
                !ELSE IF (board_ptr(i, j)%state == 1) THEN
                !    new_board_ptr(i, j)%state = 2
                ! If electron tail then becomes wire
                !ELSE IF (board_ptr(i, j)%state == 2) THEN
                !    new_board_ptr(i, j)%state = 3
                ! If wire, needs to use neightbors
                !ELSE IF (board_ptr(i, j)%state == 3) THEN
                !END IF
            END DO
        END DO

        temp => board_ptr
        board_ptr => new_board_ptr
        new_board_ptr => temp
        
    END SUBROUTINE UPDATE_CELLS

    SUBROUTINE INIT_BOARDS(board1_ptr, board2_ptr, filename)
        IMPLICIT NONE
        CHARACTER(255), INTENT(INOUT) :: filename
        INTEGER, TARGET :: x_ptr, y_ptr
        INTEGER :: x, y, i, j
        INTEGER, DIMENSION(:,:), POINTER, INTENT(INOUT) :: board1_ptr, board2_ptr
        INTEGER, DIMENSION(:,:), ALLOCATABLE, TARGET :: board1, board2
        CHARACTER(10**6) :: text
        CHARACTER(10**6), TARGET :: text_ptr


        ! board1_ptr=>board1  
        ! board2_ptr=>board2

        ! Get the dimenstions x and y from file
        ! filename = 
        CALL GET_XY(filename, x_ptr, y_ptr)

        ALLOCATE(board1_ptr(y_ptr, x_ptr))
        ALLOCATE(board2_ptr(y_ptr, x_ptr))

        DO i = 1, x_ptr
            DO j = 1, y_ptr
                board1_ptr(j,i) = 0
                board2_ptr(j,i) = 0
            END DO
        END DO

        CALL GET_FILE_TEXT(filename, text_ptr)

        CALL TEXT_TO_MODEL(board1_ptr, text_ptr)

        
    END SUBROUTINE INIT_BOARDS
END MODULE
