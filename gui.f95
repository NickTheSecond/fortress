MODULE GUI
USE CA_WW
USE FCURSES
IMPLICIT NONE

CONTAINS

SUBROUTINE DRAW(cells, scrn1, gen)
    INTEGER, DIMENSION(:,:), POINTER, INTENT(IN) :: cells
    CHARACTER(:), ALLOCATABLE, INTENT(INOUT) :: scrn1
    INTEGER, INTENT(IN) :: gen
    INTEGER, DIMENSION(2) :: cells_shape
    INTEGER :: y_max, x_max, i, j
    CHARACTER(LEN=32) :: generation_label
                                               ! char      a  fg  bg  lx ly
    TYPE(TPX), DIMENSION(4) :: colors = (/ TPX(CHAR(97),   0,  0,  0,  0, 0), & ! head
                                           TPX(CHAR(98),   7, 1, 1,  0, 0), & ! head
                                           TPX(CHAR(99),   7, 6, 6,  0, 0), & ! tail
                                           TPX(CHAR(100),  7, 7, 7,  0, 0) /) ! wire
  
    cells_shape = SHAPE(cells)
    y_max = cells_shape(1)
    x_max = cells_shape(2)

    !CALL INIT_SCREEN("/tmp")
    scrn1 = clear_screen
    WRITE(generation_label,*) "Generation", gen
    CALL SPUT(generation_label, 1, lines, scrn1)
    CALL DRAW_SCREEN(scrn1)
    DO j=1,x_max-1
        DO i=1,y_max-1
            CALL DRAWTPX(colors(cells(i, j)+1), j, i)
        END DO
    END DO
    !CALL KILL_SCREEN("/tmp")
END

SUBROUTINE SHOW_BOARD(cells, y_size, x_size)
    INTEGER, DIMENSION(:,:), POINTER, INTENT(IN) :: cells
    INTEGER, INTENT(IN) :: y_size, x_size
    INTEGER :: i, j
    
    DO i=1,y_size
        DO j=1,x_size
            WRITE(*,*) cells(i, j)
        END DO
        WRITE(*,*)
    END DO

END SUBROUTINE

SUBROUTINE EXPORT_RLE(dest_path, cells)
CHARACTER(LEN=:), ALLOCATABLE, INTENT(IN) :: dest_path
INTEGER, POINTER, DIMENSION(:,:), INTENT(IN) :: cells
INTEGER :: err, i, j, x, y, prev_state, run_length
INTEGER, DIMENSION(2) :: cells_shape

    cells_shape = SHAPE(cells)
    y = cells_shape(1)
    x = cells_shape(2)


    prev_state = -1
    run_length = 0
    OPEN(UNIT=24, FILE=dest_path, STATUS="new", ACTION="write", IOSTAT=err)
    DO i=1,y
        DO j=1,x
            IF (ERR /= 0) EXIT

            IF (cells(i, j) == prev_state) THEN 
                run_length = run_length + 1
            ELSE
                SELECT CASE(prev_state)
                    CASE(0)
                        WRITE(24, '(I3A)') run_length, "b"
                    CASE(1:11)
                        WRITE(24, '(I3A)') run_length, CHAR(ICHAR("o")+prev_state-1)
                END SELECT
                prev_state = cells(i, j)
                run_length = 1
            END IF

            IF (j==x) THEN 
                WRITE(24, '(A)') "$"
                prev_state = -1
                run_length = 0
            END IF

        END DO
    END DO
    CLOSE(24)

END SUBROUTINE

SUBROUTINE EXPORT_PGM(dest_path, cells, num_states)
CHARACTER(LEN=255), INTENT(IN) :: dest_path
INTEGER, DIMENSION(:,:), POINTER, INTENT(IN) :: cells
INTEGER, INTENT(IN) :: num_states
INTEGER :: err, i, j, x, y
INTEGER, DIMENSION(2) :: cells_shape
    cells_shape = SHAPE(cells)
    y = cells_shape(1)
    x = cells_shape(2)
    OPEN(UNIT=24, FILE=dest_path, STATUS="new", ACTION="write", IOSTAT=err)
    WRITE(24, '(A)') "P2"
    WRITE(24, '(A)') "# WireWorld"
    WRITE(24, '(I4,1X,I4)') x, y
    WRITE(24, *) num_states
    DO i=1,y
        DO j=1,x
            IF (ERR /= 0) EXIT
            
            WRITE(24, *) cells(i, j)
        END DO
    END DO
    CLOSE(24)
    WRITE(*,'(A)') "Exported image."
END SUBROUTINE

END MODULE
