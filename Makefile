all:
	gcc -c ./libs/regex/regex_helper.c
	gfortran -c ./libs/regex/regex_module.f95
	gcc -c ./libs/fcurses/kbhit.c
	gfortran -c ./libs/fcurses/fcurses.f90
	gfortran -c cell_type.f95
	gfortran -c read_ww.f95
	gfortran -c ca_ww.f95
	gfortran -c gui.f95
	gfortran fortress.f95 -o FORTRESS *.o
	#gfortran test_timing.f95 -o TIMING *.o
 
debug:
	gcc -c -g ./libs/regex/regex_helper.c
	gfortran -c -g ./libs/regex/regex_module.f95
	gcc -g -c ./libs/fcurses/kbhit.c
	gfortran -g -c ./libs/fcurses/fcurses.f90
	gfortran -g -ffpe-trap=zero,invalid,overflow,underflow -c cell_type.f95
	gfortran -g -ffpe-trap=zero,invalid,overflow,underflow -c read_ww.f95
	gfortran -g -ffpe-trap=zero,invalid,overflow,underflow -c ca_ww.f95
	gfortran -g -ffpe-trap=zero,invalid,overflow,underflow -c gui.f95
	gfortran -g fortress.f95 -ffpe-trap=zero,invalid,overflow,underflow -o FORTRESS *.o
	#gfortran -g test_timing.f95 -ffpe-trap=zero,invalid,overflow,underflow -o TIMING *.o

clean:
	rm *.o
	rm *.mod
	rm *.pgm
